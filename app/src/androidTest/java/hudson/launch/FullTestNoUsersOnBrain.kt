package hudson.launch


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import hudson.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class FullTestNoUsersOnBrain {

	@Rule
	@JvmField
	var mActivityTestRule = ActivityTestRule(LoadingActivity::class.java)

	@Test
	fun fullTestNoUsersOnBrain() {
		val viewPager = onView(
				allOf(withId(R.id.pair_ready_viewpager),
						childAtPosition(
								allOf(withId(R.id.main_content),
										childAtPosition(
												withId(android.R.id.content),
												0)),
								0),
						isDisplayed()))
		viewPager.perform(swipeLeft())

		val viewPager2 = onView(
				allOf(withId(R.id.pair_ready_viewpager),
						childAtPosition(
								allOf(withId(R.id.main_content),
										childAtPosition(
												withId(android.R.id.content),
												0)),
								0),
						isDisplayed()))
		viewPager2.perform(swipeLeft())

		// Added a sleep statement to match the app's execution delay.
		// The recommended way to handle such scenarios is to use Espresso idling resources:
		// https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
		Thread.sleep(6000)

		val textView = onView(
				allOf(withId(R.id.content), withText("Hub-N3IY"),
						childAtPosition(
								childAtPosition(
										IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
										0),
								0),
						isDisplayed()))
		textView.check(matches(withText("Hub-N3IY")))

		val recyclerView = onView(
				allOf(withId(R.id.list),
						childAtPosition(
								withClassName(`is`("android.widget.LinearLayout")),
								0)))
		recyclerView.perform(actionOnItemAtPosition<ViewHolder>(0, click()))

		val viewPager3 = onView(
				allOf(withId(R.id.pair_ready_viewpager),
						childAtPosition(
								allOf(withId(R.id.main_content),
										childAtPosition(
												withId(android.R.id.content),
												0)),
								0),
						isDisplayed()))
		viewPager3.perform(swipeLeft())

		val textView2 = onView(
				allOf(withId(R.id.brainNameText), withText("Hub-N3IY"),
						childAtPosition(
								withParent(withId(R.id.pair_ready_viewpager)),
								1),
						isDisplayed()))
		textView2.check(matches(withText("Hub-N3IY")))

		val textView3 = onView(
				allOf(withId(R.id.enterWifiTopText), withText("Some Input Needed"),
						childAtPosition(
								withParent(withId(R.id.pair_ready_viewpager)),
								0),
						isDisplayed()))
		textView3.check(matches(withText("Some Input Needed")))

		val textInputEditText = onView(
				allOf(withId(R.id.brainPasswordInput),
						childAtPosition(
								childAtPosition(
										withId(R.id.textInputLayout3),
										0),
								0),
						isDisplayed()))
		textInputEditText.perform(replaceText(""), closeSoftKeyboard())

		val textInputEditText2 = onView(
				allOf(withId(R.id.brainPasswordInput), withText(""),
						childAtPosition(
								childAtPosition(
										withId(R.id.textInputLayout3),
										0),
								0),
						isDisplayed()))
		textInputEditText2.perform(pressImeActionButton())

		// Added a sleep statement to match the app's execution delay.
		// The recommended way to handle such scenarios is to use Espresso idling resources:
		// https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
		Thread.sleep(1000)

		val appCompatEditText = onView(
				allOf(withId(R.id.loginUsernameInput),
						childAtPosition(
								childAtPosition(
										withId(R.id.loginUsernameInputLayout),
										0),
								0),
						isDisplayed()))
		appCompatEditText.perform(replaceText("Ace"), closeSoftKeyboard())

		val appCompatEditText2 = onView(
				allOf(withId(R.id.loginPasswordInput),
						childAtPosition(
								childAtPosition(
										withId(R.id.loginPasswordInputLayout),
										0),
								0),
						isDisplayed()))
		appCompatEditText2.perform(replaceText("butts"), closeSoftKeyboard())

		pressBack()

		val textView4 = onView(
				allOf(withId(R.id.enterWifiTopText), withText("You're new!"),
						childAtPosition(
								childAtPosition(
										withId(android.R.id.content),
										0),
								0),
						isDisplayed()))
		textView4.check(matches(withText("You're new!")))

		val cardView = onView(
				allOf(withId(R.id.registerButton),
						childAtPosition(
								allOf(withId(R.id.constraintLayout2),
										childAtPosition(
												withClassName(`is`("android.support.constraint.ConstraintLayout")),
												0)),
								0),
						isDisplayed()))
		cardView.perform(click())

		val textView5 = onView(
				allOf(withId(R.id.textView12), withText("You're logged in as Ace"),
						childAtPosition(
								withParent(withId(R.id.container)),
								0),
						isDisplayed()))
		textView5.check(matches(withText("You're logged in as Ace")))
	}

	private fun childAtPosition(
			parentMatcher: Matcher<View>, position: Int): Matcher<View> {

		return object : TypeSafeMatcher<View>() {
			override fun describeTo(description: Description) {
				description.appendText("Child at position $position in parent ")
				parentMatcher.describeTo(description)
			}

			public override fun matchesSafely(view: View): Boolean {
				val parent = view.parent
				return parent is ViewGroup && parentMatcher.matches(parent)
						&& view == parent.getChildAt(position)
			}
		}
	}
}
