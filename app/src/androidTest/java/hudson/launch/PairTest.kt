package hudson.launch


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import hudson.R
import hudson.launch.LoadingActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class PairTest {

	@Rule
	@JvmField
	var mActivityTestRule = ActivityTestRule(LoadingActivity::class.java)

	@Test
	fun pairTest() {
		val viewPager = onView(
				allOf(withId(R.id.pair_ready_viewpager),
						childAtPosition(
								allOf(withId(R.id.main_content),
										childAtPosition(
												withId(android.R.id.content),
												0)),
								0),
						isDisplayed()))
		viewPager.perform(swipeLeft())

		val viewPager2 = onView(
				allOf(withId(R.id.pair_ready_viewpager),
						childAtPosition(
								allOf(withId(R.id.main_content),
										childAtPosition(
												withId(android.R.id.content),
												0)),
								0),
						isDisplayed()))
		viewPager2.perform(swipeLeft())

		// Added a sleep statement to match the app's execution delay.
		// The recommended way to handle such scenarios is to use Espresso idling resources:
		// https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
		Thread.sleep(6000)

		val recyclerView = onView(
				allOf(withId(R.id.list),
						childAtPosition(
								withClassName(`is`("android.widget.LinearLayout")),
								0)))
		recyclerView.perform(actionOnItemAtPosition<ViewHolder>(0, click()))

		val viewPager3 = onView(
				allOf(withId(R.id.pair_ready_viewpager),
						childAtPosition(
								allOf(withId(R.id.main_content),
										childAtPosition(
												withId(android.R.id.content),
												0)),
								0),
						isDisplayed()))
		viewPager3.perform(swipeLeft())

		val textInputEditText = onView(
				allOf(withId(R.id.brainPasswordInput),
						childAtPosition(
								childAtPosition(
										withId(R.id.textInputLayout3),
										0),
								0),
						isDisplayed()))
		textInputEditText.perform(replaceText("kcrM2yN9LiY5"), closeSoftKeyboard())

		val textInputEditText2 = onView(
				allOf(withId(R.id.brainPasswordInput), withText("kcrM2yN9LiY5"),
						childAtPosition(
								childAtPosition(
										withId(R.id.textInputLayout3),
										0),
								0),
						isDisplayed()))
		textInputEditText2.perform(pressImeActionButton())
	}

	private fun childAtPosition(
			parentMatcher: Matcher<View>, position: Int): Matcher<View> {

		return object : TypeSafeMatcher<View>() {
			override fun describeTo(description: Description) {
				description.appendText("Child at position $position in parent ")
				parentMatcher.describeTo(description)
			}

			public override fun matchesSafely(view: View): Boolean {
				val parent = view.parent
				return parent is ViewGroup && parentMatcher.matches(parent)
						&& view == parent.getChildAt(position)
			}
		}
	}
}
