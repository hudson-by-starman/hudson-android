package hudson

import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hudson.R


class SettingsFragment: Fragment() {


    private var rootView: View? = null

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        changeBackground()
    }

    private fun changeBackground(){
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
        }
        else{
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.main_fragment_settings, container, false)
        changeBackground()
        return rootView
    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): SettingsFragment {
            val fragment = SettingsFragment()
            val args = Bundle()

            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}