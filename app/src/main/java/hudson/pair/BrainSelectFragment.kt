package hudson.pair

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.fondesa.kpermissions.extension.listeners
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.github.pwittchen.reactivewifi.AccessRequester
import com.github.pwittchen.reactivewifi.ReactiveWifi
import hudson.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.pair_fragment_brain_list.*
import kotlinx.android.synthetic.main.pair_fragment_brain_list.view.*
import java.util.*


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [BrainSelectFragment.OnListFragmentInteractionListener] interface.
 */
class BrainSelectFragment : Fragment() {
    private var SSIDS: MutableList<SsidData> = ArrayList()

    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var wifiManager: WifiManager
    private var formattedSsids: MutableList<String> = mutableListOf()

    private var foundBrain = false
	private var iconAvd: AnimatedVectorDrawable? = null
    private var rootView: View? = null

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        changeBackground()
    }
    private fun changeBackground(){
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait3)
        }
        else{
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape3)
        }
    }

    private fun permissionsCheck(){

        swiperefresh.isEnabled = false
        SSIDS.clear()
        formattedSsids.clear()

        val coarseLocationPermission = ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.ACCESS_COARSE_LOCATION)
        val accessWifiPermission = ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.ACCESS_WIFI_STATE)
        val changeWifiPermission = ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.CHANGE_WIFI_STATE)
        val internetPermission = ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.INTERNET)


        list.adapter!!.notifyDataSetChanged()

        if (coarseLocationPermission != PackageManager.PERMISSION_GRANTED || accessWifiPermission != PackageManager.PERMISSION_GRANTED || changeWifiPermission != PackageManager.PERMISSION_GRANTED || internetPermission != PackageManager.PERMISSION_GRANTED) {
            Log.d("TESTING", "Permissions not granted")

            val request = permissionsBuilder(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.INTERNET).build()

            request.send()

            request.listeners {

                onAccepted {
                    Log.d("TESTING", "All permissions granted")
                    startWifiAccessPointsSubscription()
                }

                onDenied {

                }

                onPermanentlyDenied {
                    // Notified when the permissions are permanently denied.
                }

                onShouldShowRationale { _, _ ->
                    // Notified when the permissions should show a rationale.
                    // The nonce can be used to request the permissions again.
                }
            }
        }
        else{
            Log.d("TESTING", "All permissions granted")
            startWifiAccessPointsSubscription()

        }
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            permissionsCheck()
        } else {
            safelyUnsubscribe(wifiSubscription)
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
	@SuppressLint("MissingPermission")
    private fun startWifiAccessPointsSubscription() {
        if (!AccessRequester.isLocationEnabled(context as Activity?)) {
            AccessRequester.requestLocationAccess(context as Activity?)
        }
        Handler().postDelayed({
            Log.d("TESTING", "Time ran out on SSID search")
            if (this.activity != null){
				this.activity!!.runOnUiThread {
					iconAvd!!.clearAnimationCallbacks()
				}
			}

			safelyUnsubscribe(wifiSubscription)
            if(foundBrain){
                searchingForBrainStatus?.text = getString(R.string.brainFoundRescan)

            }
            else{
                searchingForBrainStatus?.text = getString(R.string.noBrainsFound)
				this.activity!!.runOnUiThread {
					animated_loading_icon?.visibility = INVISIBLE
					sad_face?.visibility = VISIBLE

					val animatedIcon = sad_face
					Log.d("TESTING", animatedIcon.toString())

					iconAvd = animatedIcon.drawable as AnimatedVectorDrawable
					iconAvd!!.registerAnimationCallback(object : Animatable2.AnimationCallback() {})
					iconAvd!!.start()
				}
            }
            if (swiperefresh != null) {
                swiperefresh.isEnabled = true
            }
        }, 6000)

        Log.d("TESTING", "Location available")

        wifiManager = this.activity?.applicationContext?.getSystemService(Context.WIFI_SERVICE) as WifiManager

        wifiSubscription = ReactiveWifi
                .observeWifiAccessPoints(activity?.applicationContext)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { scanResults ->
                    displayAccessPoints(scanResults)
                }

    }



    private fun displayAccessPoints(scanResults: List<ScanResult>) {
        val ssids = scanResults.map { it.SSID }
        for (ssid in ssids){

            if (ssid.contains("Hudson-")) {
                if (!formattedSsids.contains(ssid)) {
                    Log.d("TESTING", "Found a Brain")
                    foundBrain = true
                    formattedSsids.add(ssid)
                    addItem(createSsidItem(ssid))
                    list.adapter!!.notifyDataSetChanged()
                }

            }
        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(
                R.layout.pair_fragment_brain_list,
                container,
                false)
        changeBackground()
        rootView!!.swiperefresh.isEnabled = false

        val recyclerView = rootView!!.list
        if (recyclerView is RecyclerView) {
            with(recyclerView) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }

                recyclerView.adapter = BrainSelectFragmentRecyclerViewAdapter(SSIDS)

            }
        }

        val animatedIcon = rootView!!.animated_loading_icon
        iconAvd = animatedIcon.drawable as AnimatedVectorDrawable
        iconAvd!!.registerAnimationCallback(
                object : Animatable2.AnimationCallback() {
                    override fun onAnimationEnd(drawable: Drawable) {
                        iconAvd!!.start()

                    }
                })
        iconAvd!!.start()

        rootView!!.swiperefresh.setOnRefreshListener {
            Log.d("TESTING", "Doing a refresh")
            //safelyUnsubscribe(wifiSubscription)
            searchingForBrainStatus?.text = getString(R.string.searchingForNearbyBrains)
            animated_loading_icon?.visibility = VISIBLE
			sad_face?.visibility = INVISIBLE
			iconAvd = animatedIcon.drawable as AnimatedVectorDrawable
			iconAvd!!.registerAnimationCallback(
					object : Animatable2.AnimationCallback() {
						override fun onAnimationEnd(drawable: Drawable) {
							iconAvd!!.start()

						}
					})
			iconAvd!!.start()
			permissionsCheck()
            rootView!!.swiperefresh.isRefreshing = false
        }


        return rootView
    }



    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnListFragmentInteractionListener

    companion object {

        fun safelyUnsubscribe(vararg subscriptions: Disposable?) {
            Log.d("TESTING", "Unsubscribing")

            subscriptions
                    .filterNotNull()
                    .filterNot { it.isDisposed }
                    .forEach { it.dispose() }
        }

        var wifiSubscription: Disposable? = null

        // TODO: Customize parameter argument names
        private const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
                BrainSelectFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }

    private fun addItem(item: SsidData) {
        SSIDS.add(item)
    }

    private fun createSsidItem(value: String): SsidData {
        return SsidData(value)
    }

    data class SsidData(val content: String) {
        override fun toString(): String = content
    }
}
