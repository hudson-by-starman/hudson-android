package hudson.pair

import android.app.Activity
import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.ImageView
import hudson.R
import kotlinx.android.synthetic.main.pair_fragment_splash.*
import android.content.Context
import android.util.Log
import kotlinx.android.synthetic.main.pair_fragment_brain_list.view.animated_loading_icon
import kotlinx.android.synthetic.main.pair_fragment_splash.view.*




class WelcomeToHudsonFragment: Fragment() {

    private var killAnimation: Boolean = false
    private var rootView: View? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        killAnimation = !isVisibleToUser
    }

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        changeBackground()
    }

	private fun changeBackground(){
		if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
			rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait1)
		}
		else{
			rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape1)
		}
	}

	@RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val newInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        rootView = newInflater.inflate(R.layout.pair_fragment_splash, container, false)
		changeBackground()
        startAnimations(rootView!!)
        return rootView
    }

    private fun startAnimations(rootView: View){

        val animatedIcon = rootView.animated_loading_icon
        val iconAvd = animatedIcon!!.drawable as AnimatedVectorDrawable
        val animatedArrow: ImageView = rootView.animated_arrow
        val arrowAvd = animatedArrow.drawable as AnimatedVectorDrawable

        iconAvd.clearAnimationCallbacks()
        arrowAvd.clearAnimationCallbacks()
        iconAvd.registerAnimationCallback(
                object : Animatable2.AnimationCallback() {
                    override fun onAnimationEnd(drawable: Drawable) {

                        Log.d("TESTING", "Done winking")

                        val letsGoText = lets_go_text
                        val fadeInAnimation = AnimationUtils.loadAnimation(activity, R.anim.fade_in)
                        fadeInAnimation.reset()

                        letsGoText!!.startAnimation(fadeInAnimation)
                        fadeInAnimation.setAnimationListener(object : Animation.AnimationListener {
                            override fun onAnimationStart(animation: Animation?) {
                                letsGoText.alpha = 1.0F
                            }
                            override fun onAnimationRepeat(animation: Animation?) {
                            }
                            override fun onAnimationEnd(arg0: Animation) {
                                animatedArrow.alpha = 1.0F
                                arrowAvd.registerAnimationCallback(
                                        object : Animatable2.AnimationCallback() {
                                            override fun onAnimationEnd(drawable: Drawable) {
                                                if(killAnimation){
                                                    iconAvd.clearAnimationCallbacks()
                                                    arrowAvd.clearAnimationCallbacks()
                                                }
                                                else{
                                                    arrowAvd.start()
                                                }
                                            }
                                        })
                                arrowAvd.start()
                            }
                        })

                    }
                })
        iconAvd.start()
    }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private const val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): WelcomeToHudsonFragment {
                val fragment = WelcomeToHudsonFragment()
                val args = Bundle()

                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }



}