package hudson.pair

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import hudson.R
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.pair_fragment_brain_item.view.*


class BrainSelectFragmentRecyclerViewAdapter(
        private val mValues: List<BrainSelectFragment.SsidData>)
    : RecyclerView.Adapter<BrainSelectFragmentRecyclerViewAdapter.ViewHolder>() {




    companion object {
        var selected_ssid: String? = null
        var disposable: Disposable? = null
    }

    private val mOnClickListener: OnClickListener = OnClickListener { v ->
        val item = v.tag as BrainSelectFragment.SsidData
        selected_ssid = item.toString()


        PairReadyActivity.SectionsPagerAdapter.fragmentSize = 4
        PairReadyActivity.mSectionsPagerAdapter!!.notifyDataSetChanged()
        PairReadyActivity.mViewPage!!.currentItem = 3
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.pair_fragment_brain_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mContentView.text = item.content

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mContentView: TextView = mView.content

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
