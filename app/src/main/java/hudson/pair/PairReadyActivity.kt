package hudson.pair

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import hudson.R
import kotlinx.android.synthetic.main.pair_activity.*
import android.view.ViewGroup
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.LayoutInflater




class PairReadyActivity : AppCompatActivity() {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.pair_activity)

        mViewPage = pair_ready_viewpager
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        //Log.d("TESTING", mSectionsPagerAdapter.toString())
        // Set up the ViewPager with the sections adapter.
        pair_ready_viewpager.adapter = mSectionsPagerAdapter
        mSectionsPagerAdapter!!.notifyDataSetChanged()
    }


    companion object {
        var mSectionsPagerAdapter: SectionsPagerAdapter? = null
        var mViewPage: ViewPager? = null
    }

    override fun onPause() {
        super.onPause()
        BrainSelectFragmentRecyclerViewAdapter.disposable?.dispose()
        BrainSelectFragment.safelyUnsubscribe(BrainSelectFragment.wifiSubscription)
    }


    class SectionsPagerAdapter(fm: FragmentManager)  : FragmentPagerAdapter(fm) {


        companion object {
            var fragmentSize: Int = 3
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 // Fragment # 0
                -> WelcomeToHudsonFragment.newInstance(position + 1)
                1 // Fragment # 1
                -> PrivacyFragment.newInstance(position + 1)
                2 // Fragment # 2
                -> BrainSelectFragment.newInstance(position + 1)
                else // Fragment # 3
                -> BrainPasswordEnterFragment.newInstance(position + 1)

            }
        }

        override fun getCount(): Int {
            return fragmentSize
        }


    }

}
