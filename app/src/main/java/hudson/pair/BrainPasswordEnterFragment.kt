package hudson.pair

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import hudson.R
import hudson.launch.LoadingActivity
import kotlinx.android.synthetic.main.pair_fragment_brain_enter_password.*
import kotlinx.android.synthetic.main.pair_fragment_brain_enter_password.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class BrainPasswordEnterFragment : Fragment() {
	private var rootView: View? = null

	override fun onConfigurationChanged(newConfig: Configuration){
		super.onConfigurationChanged(newConfig)
		changeBackground()
	}

	private fun changeBackground(){
		if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
			rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait4)
		}
		else{
			rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape4)
		}
	}

	private fun checkCurrentNetworkStatus() {
		val connectivityManager = this.activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		val wifiManager = this.activity?.applicationContext?.getSystemService(Context.WIFI_SERVICE) as WifiManager
		val activeNetworkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
		if (activeNetworkInfo != null) {
			if (activeNetworkInfo.isConnected) {
				Log.d("TESTING", "The user started the app with functional internet")
				val isWiFi: Boolean = activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI
				val isMobile: Boolean = activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE

				if (isWiFi) {
					PairActivity.startedWithWifiorMobile = 1
					val connectionInfo = wifiManager.connectionInfo
					if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.ssid)) {
						PairActivity.startingNetID = connectionInfo.networkId
					}
				}
				if (isMobile) {
					PairActivity.startedWithWifiorMobile = 2
					Log.d("TESTING", "The user is connected to Mobile Internet")
				}
			} else {
				Log.d("TESTING", "The user did not start the app with functional internet")
				PairActivity.startedWithWifiorMobile = 0
			}
		} else {
			Log.d("TESTING", "The user did not start the app with functional internet")
			PairActivity.startedWithWifiorMobile = 0
		}
	}


	override fun setUserVisibleHint(isVisibleToUser: Boolean) {
		super.setUserVisibleHint(isVisibleToUser)

		if (isVisibleToUser) {
			brainNameText.text = BrainSelectFragmentRecyclerViewAdapter.selected_ssid

		}
	}


	@RequiresApi(Build.VERSION_CODES.M)
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		var brainPasswordEntered = false

		rootView = inflater.inflate(R.layout.pair_fragment_brain_enter_password, container, false)
		changeBackground()



		rootView!!.brainPasswordInput.addTextChangedListener(object : TextWatcher {
			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
			}

			override fun afterTextChanged(s: Editable?) {
			}

			override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
				brainPasswordEntered = charSequence.length == 12
				LoadingActivity.brainPassword = charSequence.toString()

				if (brainPasswordEntered) {
					rootView!!.begin_pairing_button.visibility = View.VISIBLE

				} else {
					rootView!!.begin_pairing_button.visibility = View.GONE
				}

			}
		})


		GlobalScope.launch {
			checkCurrentNetworkStatus()
		}


		rootView!!.brainPasswordInput.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
			if ((keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP)) {
				Log.d("TESTING", "User pressed enter")
				if (brainPasswordEntered) {
					this.activity!!.registerReceiver(finishReceiver, IntentFilter("finish_activity"))
					hideKeyboard(activity!!)
					val intent = Intent(activity, PairActivity::class.java)
					startActivity(intent)
					return@OnKeyListener true

				} else {
					return@OnKeyListener false
				}
			}
			false
		})

		rootView!!.begin_pairing_button.setOnClickListener {
			this.activity!!.registerReceiver(finishReceiver, IntentFilter("finish_activity"))
			hideKeyboard(activity!!)
			val intent = Intent(activity, PairActivity::class.java)
			startActivity(intent)
		}

		rootView!!.brainNameText.text = BrainSelectFragmentRecyclerViewAdapter.selected_ssid
		return rootView
	}



	private fun hideKeyboard(activity: FragmentActivity) {
		val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
		var view = activity.currentFocus

		if (view == null) {
			view = View(activity)
		}
		imm.hideSoftInputFromWindow(view.windowToken, 0)
	}

	private fun Context.toast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


	//Receiver gets a message when the Brain has successfully connected
	//The receiver closes this activity
	//The activity doesn't close until then, because if the password is incorrect, we return to this activity
	private var finishReceiver: BroadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			val action = intent.action
			if (action == "finish_activity") {
				activity?.finish()
				context.unregisterReceiver(this)
			}
		}
	}



	companion object {

		private const val ARG_SECTION_NUMBER = "section_number"
		fun newInstance(sectionNumber: Int): BrainPasswordEnterFragment {
			val fragment = BrainPasswordEnterFragment()
			val args = Bundle()

			args.putInt(ARG_SECTION_NUMBER, sectionNumber)
			fragment.arguments = args
			return fragment
		}
	}

}