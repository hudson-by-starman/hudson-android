package hudson.pair

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import hudson.R




class WifiNetworkRecyclerViewAdapter constructor(val context: Context, private val list: List<WifiData>, val clickListener: (WifiData) -> Unit): RecyclerView.Adapter<WifiNetworkRecyclerViewAdapter.WifiViewHolder>(){


	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WifiViewHolder {
		val inflater = LayoutInflater.from(context)
		return WifiViewHolder(inflater, parent)
	}

	override fun onBindViewHolder(holder: WifiViewHolder, position: Int) {
		val wifiData: WifiData = list[position]
		holder.bind(wifiData)
		holder.networkSSIDCardView!!.setOnClickListener {
			clickListener(list[position])
		}
	}

	override fun getItemCount(): Int = list.size

	inner class WifiViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
			RecyclerView.ViewHolder(inflater.inflate(R.layout.pair_received_wifi_list_item,
					parent, false)) {

		var networkSSIDCardView: CardView? = null

		init {
			networkSSIDCardView = itemView.findViewById(R.id.networkSSIDCardView)
		}

		fun bind(wifiData: WifiData) {
			val child: TextView = networkSSIDCardView?.getChildAt(0) as TextView
			child.text = wifiData.ssid
		}

	}
}

data class WifiData(var ssid: String, var encrypted: Boolean, var encryptionType: String?)
fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
	itemView.setOnClickListener {
		event.invoke(adapterPosition, itemViewType)
	}
	return this
}


