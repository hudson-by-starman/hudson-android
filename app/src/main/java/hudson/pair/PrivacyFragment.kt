package hudson.pair

import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hudson.R
import kotlinx.android.synthetic.main.pair_fragment_permissions.*
import kotlinx.android.synthetic.main.pair_fragment_permissions.view.*

class PrivacyFragment: Fragment() {


    private var killArrowAnimation: Boolean = false
    private var rootView: View? = null

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        changeBackground()
    }
    private fun changeBackground(){
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
        }
        else{
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
        }
    }
    @RequiresApi(Build.VERSION_CODES.M)
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        killArrowAnimation = !isVisibleToUser
        if(isVisibleToUser){
            restartAnimation()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun restartAnimation(){
        val animatedArrow = animated_arrow2
        val arrowAvd = animatedArrow.drawable as AnimatedVectorDrawable

        arrowAvd.clearAnimationCallbacks()

        arrowAvd.registerAnimationCallback(
                @RequiresApi(Build.VERSION_CODES.M)
                object : Animatable2.AnimationCallback() {
                    override fun onAnimationEnd(drawable: Drawable) {
                        if(killArrowAnimation){
                            arrowAvd.clearAnimationCallbacks()
                        }
                        else{
                            arrowAvd.start()
                        }
                    }
                })
        arrowAvd.start()
    }
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.pair_fragment_permissions, container, false)
        changeBackground()
        val animatedArrow = rootView!!.animated_arrow2
        val arrowAvd = animatedArrow.drawable as AnimatedVectorDrawable

        arrowAvd.clearAnimationCallbacks()

        arrowAvd.registerAnimationCallback(
                @RequiresApi(Build.VERSION_CODES.M)
                object : Animatable2.AnimationCallback() {
                    override fun onAnimationEnd(drawable: Drawable) {
                        if(killArrowAnimation){
                            arrowAvd.clearAnimationCallbacks()
                        }
                        else{
                            arrowAvd.start()
                        }
                    }
                })
        arrowAvd.start()

        return rootView
    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): PrivacyFragment {
            val fragment = PrivacyFragment()
            val args = Bundle()

            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}