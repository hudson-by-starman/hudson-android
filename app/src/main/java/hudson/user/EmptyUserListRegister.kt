package hudson.user

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.core.Json
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import hudson.R
import hudson.launch.AppDatabase
import hudson.launch.CertConfigErrorActivity
import hudson.launch.LoadingActivity
import kotlinx.android.synthetic.main.user_fragment_login_register.*
import java.lang.Exception
import kotlin.concurrent.thread


class EmptyUserListRegister : AppCompatActivity() {

	var username: String? = null
	var password: String? = null
	var formattedUserList: MutableList<String> = mutableListOf()
	private var usernameEntered: Boolean = false
	private val loggingIn = false

	override fun onConfigurationChanged(newConfig: Configuration){
		super.onConfigurationChanged(newConfig)
		changeBackground()
	}

	private fun changeBackground(){
		if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
			userRegisterLoginLayout.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
		}
		else{
			userRegisterLoginLayout.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
		}
	}


	@TargetApi(Build.VERSION_CODES.M)
	@RequiresApi(Build.VERSION_CODES.M)
	override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_fragment_login_register)
		changeBackground()
		val icon = animated_loading_icon_login
		val iconAvd = icon.drawable as AnimatedVectorDrawable
		iconAvd.registerAnimationCallback(
				object : Animatable2.AnimationCallback() {
					override fun onAnimationEnd(drawable: Drawable) {
						iconAvd.start()
					}
				})
		iconAvd.start()

		loginUsernameInput.addTextChangedListener(object : TextWatcher {
			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
			}
			override fun afterTextChanged(s: Editable?) {
			}
			override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
				username = charSequence.toString()
				when {
					formattedUserList.contains(username!!) -> {
						toast("That username is taken")
						registerButton.visibility = GONE
						usernameEntered = false
					}
					charSequence.length in 1..12 -> {
						registerButton.visibility = VISIBLE
						usernameEntered = true
					}
					else -> {
						registerButton.visibility = GONE
						usernameEntered = false
					}
				}
			}
		})

		loginPasswordInput.addTextChangedListener(object : TextWatcher {
			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
			}
			override fun afterTextChanged(s: Editable?) {
			}
			override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
				password = charSequence.toString()

				if ( (usernameEntered && !loggingIn) ||  (charSequence.length in 1..12 && loggingIn)  ){
					registerButton.visibility = VISIBLE
				}
				else{
					registerButton.visibility = GONE
				}
			}
		})

		loginPasswordInput.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
			if ((keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP)) {
				Log.d("TESTING", "User pressed enter")
				dataEntered()
				return@OnKeyListener true
			}
			false
		})


		registerButton.setOnClickListener{
			dataEntered()
		}

    }

	private fun dataEntered() {

		hideKeyboard()
		thread(start = true) {
			Log.d("TESTING", "Attempting to send registration data")
			if (password == null || password == "") {
				val (_, _, userRegisterResult) = Fuel.post("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/create_user/?format=json", listOf("username" to "$username", "email" to "no@no.no", "password" to "none")).responseJson()
				handleRegisterResponse(userRegisterResult)
			} else {
				val (_, _, userRegisterResult) = Fuel.post("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/create_user/?format=json", listOf("username" to "$username", "email" to "yes@yes.yes", "password" to "$password", "has_password" to "yes")).responseJson()
				handleRegisterResponse(userRegisterResult)
			}
		}
	}

	private fun Activity.hideKeyboard() {
		hideKeyboard( if (currentFocus == null) View(this) else currentFocus )
	}

	private fun Context.hideKeyboard(view: View) {
		val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
		inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
	}

	private fun handleRegisterResponse(userRegisterResult: Result<Json, FuelError>){
		userRegisterResult.fold(
				success = {
					Log.d("TESTING", "Register success")

					val parser = Parser()
					val userRegisterResultStringBuilder = StringBuilder(userRegisterResult.get().content)
					val userRegisterResultJson = parser.parse(userRegisterResultStringBuilder) as JsonObject
					val token = userRegisterResultJson.string("token")

					val db = AppDatabase.getInstance(this)
					db?.loginDAO()?.resetLoggedInUser()

					val loginValues = LoginDB()
					loginValues.loggedIn = 1
					loginValues.token = token!!
					loginValues.user = username!!
					db?.loginDAO()?.updateLoggedInUser(loginValues)
					val loginData = db?.loginDAO()?.getLoggedInUser()
					Log.d("TESTING", "User saved as:" + loginData!!)
					val intent = Intent(this, LoadingActivity::class.java)
					this.startActivity(intent)
					finish()
				}
		) {
			Log.d("TESTING", "Register failure $userRegisterResult")
			handleFailedConnection(it.exception)
		}

	}
	private fun handleFailedConnection(exception: Exception){
		// Handle when the app tries to connect to the Brain and fails
		val ex = exception.javaClass.toString()
		if (ex == "class java.net.NoRouteToHostException" ||
				ex == "class java.net.SocketTimeoutException" ||
				ex == "class java.net.ConnectException" ||
				ex == "class java.net.SocketException"){

			Log.d("TESTING", "No route to host on either IP")
			val intent = Intent(this, CertConfigErrorActivity::class.java)
			intent.putExtra("error",resources.getString(R.string.noConnectionToBrain))
			this.startActivity(intent)
			finish()
		}
		if (ex == "class javax.net.ssl.SSLHandshakeException" || ex == "class java.security.cert.CertPathValidatorException"){
			Log.d("TESTING", "SSL certs incorrectly configured")
			val intent = Intent(this, CertConfigErrorActivity::class.java)
			intent.putExtra("error",resources.getString(R.string.sslCertBroken))
			this.startActivity(intent)
			finish()
		}

		Log.d("TESTING", "Got exception $ex")
	}
	private fun Context.toast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

}
