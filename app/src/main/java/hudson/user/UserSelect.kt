package hudson.user


import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hudson.R
import hudson.user.UserActivity.Companion.users
import kotlinx.android.synthetic.main.user_fragment_user_list.view.*


class UserSelect : Fragment() {

    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null
    private var rootView: View? = null

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        changeBackground()
    }

    interface OnListFragmentInteractionListener

    private fun changeBackground(){
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
        }
        else{
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.user_fragment_user_list, container, false)
        changeBackground()
        rootView!!.createUserButton.setOnClickListener {
            loggingIn = false
            UserActivity.SectionsPagerAdapter.fragmentSize = 2
            UserActivity.mSectionsPagerAdapter!!.notifyDataSetChanged()
            UserActivity.viewPage?.currentItem = 1
        }

        val recyclerView = rootView!!.userList

        if (recyclerView is RecyclerView) {
            with(recyclerView) {
                layoutManager = when {
                    columnCount <= 1 -> android.support.v7.widget.LinearLayoutManager(context)
                    else -> android.support.v7.widget.GridLayoutManager(context, columnCount)
                }
                recyclerView.adapter = UserSelectAdapter(users, listener)
            }
        }
        return rootView
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"

        var loggingIn: Boolean = true
        fun newInstance(sectionNumber: Int): UserSelect {
            val fragment = UserSelect()
            val args = Bundle()

            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}
