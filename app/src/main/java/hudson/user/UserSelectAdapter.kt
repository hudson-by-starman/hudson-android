package hudson.user


import android.os.AsyncTask
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import hudson.R
import hudson.launch.AppDatabase
import hudson.user.UserActivity.Companion.usersArray
import hudson.user.UserSelect.Companion.loggingIn
import kotlinx.android.synthetic.main.user_fragment_user_item.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking


class UserSelectAdapter(
        private val mValues: List<UserActivity.UsersData>,
        private val mListener: UserSelect.OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<UserSelectAdapter.ViewHolder>() {



    private val mOnClickListener: View.OnClickListener = View.OnClickListener { v ->
        loggingIn = true
        val item = v.tag as UserActivity.UsersData
        UserRegisterLogin.username = item.toString()
        Log.d("TESTING", "$item selected")

        runBlocking(Dispatchers.Default) {

            val db = AppDatabase.getInstance(v.context)
            val dbUser = db?.loginDAO()?.getUser(item.toString())
            if (dbUser!!.isNotEmpty()) {
                Log.d("TESTING", "User is already in the database")
                currentUserInDB = true
                currentUserToken = dbUser[0].token
            }
            else {
                currentUserInDB = false
                Log.d("TESTING", "User is not in the database")
                val users = usersArray!!.string("username")
                val hasPassword = usersArray!!.string("email")
                for ((count, user) in users.withIndex()) {
                    if (user == item.toString()) {
                        currentHasPassword = hasPassword[count] == "yes@yes.yes"

                        Log.d("TESTING", "Selected user is: $item. User has password: $currentHasPassword")
                        break
                    }
                }
            }
        }

        Log.d("TESTING", "Switching to next screen")
        UserActivity.SectionsPagerAdapter.fragmentSize = 2
        UserActivity.mSectionsPagerAdapter!!.notifyDataSetChanged()
        UserActivity.viewPage?.currentItem = 1

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.user_fragment_user_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mContentView.text = item.content

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mContentView: TextView = mView.user_content

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }
    }


    companion object {
        var currentHasPassword: Boolean = false
        var currentUserInDB: Boolean = false
        var currentUserToken: String? = null

    }
}
