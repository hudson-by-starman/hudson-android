package hudson.launch

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import com.beust.klaxon.JsonArray
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import hudson.MainScreenActivity
import hudson.R
import hudson.pair.PairActivity
import hudson.pair.PairReadyActivity
import hudson.user.EmptyUserListRegister
import hudson.user.LoginDB
import hudson.user.UserActivity
import hudson.user.UserActivity.Companion.formattedUserList
import hudson.user.UserActivity.Companion.usersArray
import kotlinx.android.synthetic.main.launch_activity_loading.*
import java.io.*
import java.lang.Exception
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.*
import kotlin.concurrent.thread


class LoadingActivity : AppCompatActivity() {


	private var brainSSID: String? = null
	private var users: MutableList<UserActivity.UsersData> = ArrayList()

	override fun onConfigurationChanged(newConfig: Configuration){
		super.onConfigurationChanged(newConfig)
		changeBackground()
	}
	private fun changeBackground(){
		if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
			loadingActivityLayout.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
		}
		else{
			loadingActivityLayout.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
		}
	}

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.launch_activity_loading)
		changeBackground()
		val animatedIcon = animated_loading_icon_loading_activity
		val iconAvd = animatedIcon.drawable as AnimatedVectorDrawable
		iconAvd.registerAnimationCallback(
				object : Animatable2.AnimationCallback() {
					override fun onAnimationEnd(drawable: Drawable) {
						iconAvd.start()

					}
				})
		iconAvd.start()


		readBrainData()

    }

	//This function reads any saved data about an existing brain and handles whether or not the data exists
	private fun readBrainData(){
		thread(start = true) {
			val db = AppDatabase.getInstance(this)
			val brainData = db?.storedBrainDAO()?.getAll()

			if (brainData == null || brainData.isEmpty()){
				Log.d("TESTING", "No Brain Data saved on device")
				val intent = Intent(this, PairReadyActivity::class.java)
				this.startActivity(intent)
				finish()
			}
			if(brainData!!.isNotEmpty()){
				Log.d("TESTING", "Brain Data saved on device")
				externalIP = brainData[brainData.lastIndex].externalIP
				localIP = brainData[brainData.lastIndex].localIP
				brainPassword = brainData[brainData.lastIndex].brainPassword
				brainIdentifierCode = brainData[brainData.lastIndex].brainID
				port = brainData[brainData.lastIndex].port
				brainSSID = brainData[brainData.lastIndex].brainSSID

				Log.d("TESTING", "Brain SSID is: $brainSSID")
				Log.d("TESTING", "brainIdentifierCode is: $brainIdentifierCode")

				val loginData = db.loginDAO().getLoggedInUser()

				checkIfUserExists(loginData)
			}
		}
	}

	private fun checkIfUserExists(loginData: List<LoginDB>){
		if(loginData.isEmpty()){
			Log.d("TESTING", "No user logged in")
			loggedInUser = null
			loggedInUserToken = null
		}
		else{
			Log.d("TESTING", "User logged in $loggedInUser")
			loggedInUser = loginData[0].user
			loggedInUserToken = loginData[0].token
		}

		checkCurrentNetworkStatus()
	}

	private fun checkCurrentNetworkStatus() {


		val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		val wifiManager = this.applicationContext?.getSystemService(Context.WIFI_SERVICE) as WifiManager
		val activeNetworkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
		if (activeNetworkInfo != null) {
			if (activeNetworkInfo.isConnected) {


				Log.d("TESTING", "The user started the app with functional internet")
				//val isWiFi = activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI
				val network = connectivityManager.activeNetwork
				val capabilities = connectivityManager.getNetworkCapabilities(network)
				val isWiFi = capabilities != null && (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
				if (isWiFi) {
					val connectionInfo = wifiManager.connectionInfo
					if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.ssid)) {
						var startingSSID = connectionInfo.ssid

						// Remove surrounding quotes
						startingSSID = startingSSID.substring(1, startingSSID.length -1)

						Log.d("TESTING", "The user is connected to Wifi on $startingSSID")
						prepareCheckConnections(startingSSID)
					}
				}
				else{
					prepareCheckConnections("")
				}
			}
			else {
				Log.d("TESTING", "The user did not start the app with functional internet")
				val intent = Intent(this, NoNetworkErrorActivity::class.java)
				this.startActivity(intent)
				finish()
			}
		}
		else {
			Log.d("TESTING", "The user did not start the app with functional internet")
			val intent = Intent(this, NoNetworkErrorActivity::class.java)
			this.startActivity(intent)
			finish()
		}
	}

    private fun prepareCheckConnections(startingSSID: String) {

        if (sslSocket == null){
            val keyDirectory = this.getDir("keys", MODE_PRIVATE)
			val caFile = File(keyDirectory, "externalCa.crt")
			val clientFile = File(keyDirectory, "externalClient.p12")
			if (!checkIfCertsExist(caFile, clientFile)){
				return
			}
			installCert(caFile, clientFile)
		}


        FuelManager.instance.hostnameVerifier = NullHostNameVerifier()
        FuelManager.instance.socketFactory = sslSocket!!

		if (startingSSID == brainSSID){
			checkConnections(localIP!!, port!!)
		}
		else{
			checkConnections(externalIP!!, port!!)
		}
    }

	inner class NullHostNameVerifier : HostnameVerifier {

		override fun verify(hostname: String, session: SSLSession): Boolean {
			Log.i("RestUtilImpl", "Approving certificate for $hostname")
			return externalIP!!.contains(hostname) || localIP!!.contains(hostname)
		}

	}
	private fun checkIfCertsExist(certFile: File, clientFile: File): Boolean{
		if(!(certFile.exists() && clientFile.exists())) {
			// File not found
			Log.d("TESTING", "The client or CA file was not found")
			val intent = Intent(this, CertConfigErrorActivity::class.java)
			intent.putExtra("error",resources.getString(R.string.certFileNotFound))
			this.startActivity(intent)
			finish()
			return false
		}
		return true
	}
	private fun installCert(certFile: File, clientFile: File) {
		val ks = KeyStore.getInstance("pkcs12")
		val fis = FileInputStream(clientFile)
		val pwd = brainPassword?.toCharArray()


		try {
			ks.load(fis, pwd)
		} catch (ioException: IOException) {
			Log.d("TESTING", "SSL certs incorrectly configured. Exception $ioException")
			val intent = Intent(this, CertConfigErrorActivity::class.java)
			this.startActivity(intent)
			finish()
		}


		val kmf = KeyManagerFactory.getInstance("PKIX")
		kmf.init(ks, pwd)

		val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
		val caInput: InputStream = BufferedInputStream(FileInputStream(certFile))
		val ca: X509Certificate = caInput.use { cf.generateCertificate(it) as X509Certificate }
		System.out.println("ca=" + ca.subjectDN)

		val keyStoreType = KeyStore.getDefaultType()
		val keyStore = KeyStore.getInstance(keyStoreType).apply {
			load(null, null)
			setCertificateEntry("ca", ca)
		}

		val tmfAlgorithm: String = TrustManagerFactory.getDefaultAlgorithm()
		val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm).apply { init(keyStore) }

		HttpsURLConnection.setDefaultHostnameVerifier(NullHostNameVerifier())
		val context = SSLContext.getInstance("TLS").apply { init(kmf.keyManagers, tmf.trustManagers, null) }
		PairActivity.sslSocket = context.socketFactory
		HttpsURLConnection.setDefaultSSLSocketFactory(context.socketFactory)
		sslSocket = context.socketFactory

	}

	private fun checkConnections(IP: String, port: Int){
		Log.d("TESTING", "Checking connection on $IP:$port")

		"$IP:$port/hudson_identifier_api/hudson_identifier/?format=json".httpGet().responseJson { _, _, result ->
			//do something with response
			when (result) {
				is Result.Failure -> {
					handleFailedConnection(result.getException().exception)
				}
				is Result.Success -> {
					val parser = Parser()
					val stringBuilder = StringBuilder(result.get().content)
					val dataJson = parser.parse(stringBuilder) as JsonArray<*>
					val brainID = (dataJson.string("hudsonIdentifierCode"))[0]
					Log.d("TESTING", "brainIdentifierCode sent by brain is: $brainID")

					if (brainIdentifierCode == brainID){
						Log.d("TESTING", "Got successful connection ")
						workingIP = IP
						if(loggedInUser != null){
							val intent = Intent(this, MainScreenActivity::class.java)
							this.startActivity(intent)
							finish()
						}
						else{
							getUsers()
						}
					}
					else{
						// The Brain ID is incorrect somehow
						val intent = Intent(this, CertConfigErrorActivity::class.java)
						intent.putExtra("error",resources.getString(R.string.brainIDIncorrect))
						this.startActivity(intent)
						finish()
					}
				}
			}
		}
	}

	private fun handleFailedConnection(exception: Exception){
		// Handle when the app tries to connect to the Brain and fails
		val ex = exception.javaClass.toString()
		if (ex == "class java.net.NoRouteToHostException" ||
				ex == "class java.net.SocketTimeoutException" ||
				ex == "class java.net.ConnectException" ||
				ex == "class java.net.SocketException"){

			Log.d("TESTING", "No route to host on either IP")
			val intent = Intent(this, CertConfigErrorActivity::class.java)
			intent.putExtra("error",resources.getString(R.string.noConnectionToBrain))
			this.startActivity(intent)
			finish()
		}
		if (ex == "class javax.net.ssl.SSLHandshakeException" || ex == "class java.security.cert.CertPathValidatorException"){
			Log.d("TESTING", "SSL certs incorrectly configured")
			val intent = Intent(this, CertConfigErrorActivity::class.java)
			intent.putExtra("error",resources.getString(R.string.sslCertBroken))
			this.startActivity(intent)
			finish()
		}

		Log.d("TESTING", "Got exception $ex")
	}
	private fun getUsers(){
		// Request a list of users from the server and redirect to the correct page

		thread(start=true) {
			Log.d("TESTING", "Getting User list on $workingIP")

			var users: JsonArray<String?>?
			"$workingIP:$port/users_api/users/?format=json".
					httpGet().responseJson { _, _, result ->
				when (result) {
					is Result.Failure -> {
						handleFailedConnection(result.getException().exception)
					}
					is Result.Success -> {
						val parser = Parser()
						val usersStringBuilder = StringBuilder(result.get().content)
						usersArray = parser.parse(usersStringBuilder) as JsonArray<*>
						users = usersArray!!.string("username")
						Log.d("TESTING", "$users")


						if(users == null || users!!.isEmpty()){
							//There are no users in the Hudson device, therefore this user is first, and is being made an Admin
							val intent = Intent(this, EmptyUserListRegister::class.java)
							this.startActivity(intent)
							finish()
						}
						else {
							for (user in users!!) {
								if (!formattedUserList.contains(user)) {
									addItem(createUser(user!!))
									formattedUserList.add(user)
								}
							}
							Log.d("TESTING", formattedUserList.toString())
							val intent = Intent(this, UserActivity::class.java)
							this.startActivity(intent)
							finish()
						}
					}
				}
			}

		}
	}

	private fun addItem(item: UserActivity.UsersData) {
		UserActivity.users.add(item)
	}


	private fun createUser(value: String): UserActivity.UsersData {
		return UserActivity.UsersData(value)
	}


    companion object {
        var externalIP: String? = null
        var localIP: String? = null
        var brainPassword: String? = null
		var brainIdentifierCode: String? = null
		var sslSocket: SSLSocketFactory? = null
		var loggedInUser: String? = null
        var loggedInUserToken: String? = null
		var workingIP: String? = null
		var port: Int? = null

	}
}
