package hudson.launch

import android.arch.persistence.room.*
import android.content.Context
import hudson.user.LoginDB
import hudson.user.LoginDBInterface


@Entity(tableName = "StoredBrains")
class EntityStoredBrain {
    @PrimaryKey
    var brainID: String = ""
    var brainName: String = ""
    var externalIP: String = ""
    var localIP: String = ""
    var brainSSID: String = ""
    var brainPassword: String = ""
    var port: Int = 0
}

@Dao
interface EntityStoredBrainInterface {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBrain(entityStoredBrain : EntityStoredBrain)

    @Query("SELECT * from StoredBrains")
    fun getAll(): List<EntityStoredBrain>

}


@Database(entities = [(EntityStoredBrain::class), (LoginDB::class)], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun storedBrainDAO(): EntityStoredBrainInterface

    abstract fun loginDAO(): LoginDBInterface

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java, "brains.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}